import { ApiProperty } from '@nestjs/swagger';

export class OperationResponseDto<T> {
  @ApiProperty()
  success: boolean;

  @ApiProperty()
  message?: string;

  @ApiProperty()
  response?: T | any;
}
