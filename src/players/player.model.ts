import { prop, Ref } from '@typegoose/typegoose';
import { Team } from '../teams/team.model';

export class Player {
  @prop({ required: true })
  firstName: string;

  @prop({ required: true })
  lastName: string;

  @prop()
  patronymicName: string;

  @prop()
  citizenship: string;

  @prop()
  gender: string;

  @prop()
  birthDate: string;

  @prop()
  photoUrl: string;

  @prop()
  position: string;

  @prop()
  overall: number;

  @prop()
  pac; // pace: the speed of player

  @prop()
  sho; // shooting: The player's shooting ability

  @prop()
  pas; // Passing: A player's passing ability

  @prop()
  dri; // Dribbling: A player's dribblinb ability

  @prop()
  def; // Defending: A player's all-round defensive abilities

  @prop()
  phy; // Physical: A player's overall physical attributes

  @prop({ ref: () => Team })
  teamId: Ref<Team>;
}
