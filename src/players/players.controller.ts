import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Patch,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import { PlayersService } from './players.service';
// import { Player } from './player.schema';
import { Player } from './player.model';
import { CreatePlayerDto } from './dto/create-player.dto';
import { ApiOperation, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { OperationResponseDto } from '../shared/dto/operation.response.dto';

@ApiTags('Players')
@Controller('players')
export class PlayersController {
  constructor(private playersService: PlayersService) {}

  @Get()
  async getAllPlayers(
    @Query() paginationQuery,
  ): Promise<{ count: number; data: Player[] }> {
    return this.playersService.getAllPlayers();
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Get player by id' })
  @ApiCreatedResponse({ type: OperationResponseDto })
  getPlayerById(
    @Param('id') id: string,
  ): Promise<OperationResponseDto<Player>> {
    return this.playersService.getPlayerById(id);
  }

  @Post()
  @ApiOperation({ summary: 'Create new player' })
  @ApiCreatedResponse({ type: CreatePlayerDto })
  addNewPlayer(
    @Body() createPlayerDto: CreatePlayerDto,
  ): Promise<OperationResponseDto<Player>> {
    return this.playersService.addNewPlayer(createPlayerDto);
  }

  @Patch(':id')
  editPlayer(
    @Param('id') id: string,
    @Body() createPlayerDto: CreatePlayerDto,
  ): Promise<Player> {
    return this.playersService.updatePlayer(id, createPlayerDto);
  }

  @Delete(':id')
  deletePlayer(@Param('id') id: string): Promise<{ success: boolean }> {
    return this.playersService.deletePlayer(id);
  }
}
