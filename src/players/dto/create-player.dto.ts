import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreatePlayerDto {
  @ApiProperty()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty({ required: false })
  patronymicName: string;

  @ApiProperty({ required: false })
  citizenship: string;

  @ApiProperty({ required: false })
  gender: string;

  @ApiProperty({ required: false })
  birthDate?: string;

  @ApiProperty({ required: false })
  position: string;

  @ApiProperty({ required: false })
  overall: number;

  @ApiProperty({ required: false })
  pac: number; // pace: the speed of player

  @ApiProperty({ required: false })
  sho: number; // shooting: The player's shooting ability

  @ApiProperty({ required: false })
  pas: number; // Passing: A player's passing ability

  @ApiProperty({ required: false })
  dri: number; // Dribbling: A player's dribblinb ability

  @ApiProperty({ required: false })
  def: number; // Defending: A player's all-round defensive abilities

  @ApiProperty({ required: false })
  phy: number; // Physical: A player's overall physical attributes

  @ApiProperty({ required: false })
  photoUrl?: string;
}
