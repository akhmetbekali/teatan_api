import { Module } from '@nestjs/common';
// import { MongooseModule } from '@nestjs/mongoose';
import { TypegooseModule } from 'nestjs-typegoose';
import { PlayersController } from './players.controller';
import { PlayersService } from './players.service';
// import { Player, PlayerSchema } from './player.schema';
import { Player } from './player.model';

@Module({
  imports: [
    TypegooseModule.forFeature([Player]),
    // MongooseModule.forFeature([{ name: Player.name, schema: PlayerSchema }]),
  ],
  controllers: [PlayersController],
  providers: [PlayersService],
})
export class PlayersModule {}
