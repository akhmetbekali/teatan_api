import { Injectable } from '@nestjs/common';
// import { InjectModel } from '@nestjs/mongoose';
import { InjectModel } from 'nestjs-typegoose';
import { Model } from 'mongoose';
// import { Player } from './player.schema';
import { Player } from './player.model';
import { OperationResponseDto } from '../shared/dto/operation.response.dto';
import { ReturnModelType } from '@typegoose/typegoose';

@Injectable()
export class PlayersService {
  constructor(
    @InjectModel(Player)
    private readonly playerModel: ReturnModelType<typeof Player>,
  ) {}

  async addNewPlayer(body: any): Promise<OperationResponseDto<Player>> {
    const createdPlayer = new this.playerModel(body);
    return {
      success: true,
      response: await createdPlayer.save(),
    };
  }

  async getAllPlayers(): Promise<{ count: number; data: Player[] }> {
    // const data = await this.playerModel.find().exec();
    const data = await this.playerModel.aggregate([
      {
        $lookup: {
          from: 'teams',
          localField: 'teamId',
          foreignField: '_id',
          as: 'teamId',
        },
      },
    ]);
    const count = await this.playerModel.countDocuments();
    return { data, count };
  }

  async getPlayerById(id: string): Promise<OperationResponseDto<Player>> {
    return {
      success: true,
      response: await this.playerModel.findById({ _id: id }),
    };
  }

  async updatePlayer(id: string, body: any): Promise<Player> {
    return this.playerModel.findOneAndUpdate({ _id: id }, body, { new: true });
  }

  async deletePlayer(id: string): Promise<{ success: boolean }> {
    await this.playerModel.deleteOne({ _id: id });
    return { success: true };
  }
}
