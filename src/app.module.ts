import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { PlayersModule } from './players/players.module';
import { TeamsModule } from './teams/teams.module';
import * as dotenv from 'dotenv';

try {
  if (!(process.env.DB_PASS && process.env.DB_NAME)) {
    dotenv.config();
  }
} catch (err) {
  console.log('Config error');
}

@Module({
  imports: [
    TypegooseModule.forRoot(
      `mongodb+srv://be-service:${process.env.DB_PASS}@cluster0.etnhy.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`,
      { useNewUrlParser: true, useUnifiedTopology: true },
    ),
    PlayersModule,
    TeamsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
