import { ApiProperty } from '@nestjs/swagger';

export class AttachPlayerToTeamDto {
  @ApiProperty()
  playerId: string;

  @ApiProperty()
  teamId: string;
}
