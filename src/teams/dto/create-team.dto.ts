import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateTeamDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ required: false })
  place: number;

  @ApiProperty({ required: false })
  play: number;

  @ApiProperty({ required: false })
  win: number;

  @ApiProperty({ required: false })
  draw: number;

  @ApiProperty({ required: false })
  lose: number;

  @ApiProperty({ required: false })
  gf: number;

  @ApiProperty({ required: false })
  ga: number;

  @ApiProperty({ required: false })
  pts: number;

  @ApiProperty({ required: false })
  logoUrl?: string;
}
