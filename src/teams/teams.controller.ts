import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { TeamsService } from './teams.service';
import { Team } from './team.model';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AttachPlayerToTeamDto } from './dto/attach-player-to-taem.dto';
import { OperationResponseDto } from '../shared/dto/operation.response.dto';
import { CreateTeamDto } from './dto/create-team.dto';

@ApiTags('Teams')
@Controller('teams')
export class TeamsController {
  constructor(private teamSearvice: TeamsService) {}

  @Get()
  async getAllTeams(): Promise<{ count: number; data: Team[] }> {
    return this.teamSearvice.getAllTeams();
  }

  @Post()
  @ApiOperation({ summary: 'Craete new team' })
  async addNewPlayer(@Body() team: CreateTeamDto): Promise<Team> {
    return this.teamSearvice.addNewTeam(team);
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Get team by id' })
  async getTeamById(
    @Param('id') id: string,
  ): Promise<OperationResponseDto<Team>> {
    return this.teamSearvice.getTeamById(id);
  }

  @Post('/add-player')
  @ApiOperation({ summary: 'Attach player to team' })
  async attachPlayerToTeam(@Body() body: AttachPlayerToTeamDto): Promise<any> {
    return this.teamSearvice.foo(body);
  }
}
