import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { TeamsController } from './teams.controller';
import { Team } from './team.model';
import { Player } from '../players/player.model';
import { TeamsService } from './teams.service';
import { PlayersService } from '../players/players.service';

@Module({
  imports: [TypegooseModule.forFeature([Team, Player])],
  controllers: [TeamsController],
  providers: [TeamsService, PlayersService],
})
export class TeamsModule {}
