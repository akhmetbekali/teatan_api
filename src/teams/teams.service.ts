import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';
import { Team } from './team.model';
import { Player } from '../players/player.model';
import { OperationResponseDto } from '../shared/dto/operation.response.dto';
import { AttachPlayerToTeamDto } from './dto/attach-player-to-taem.dto';
import { timeStamp } from 'console';
import { CreateTeamDto } from './dto/create-team.dto';

@Injectable()
export class TeamsService {
  constructor(
    @InjectModel(Team)
    private readonly teamModel: ReturnModelType<typeof Team>,
    @InjectModel(Player)
    private readonly playerModel: ReturnModelType<typeof Player>,
  ) {}

  async addNewTeam(body: CreateTeamDto): Promise<any> {
    const createdTeam = new this.teamModel(body);
    return {
      success: true,
      response: await createdTeam.save(),
    };
  }

  async unattachPlayerFromTeam(playerId: string): Promise<any> {
    const player = await this.playerModel.findById({ _id: playerId });

    if (player.teamId) {
      const team = await this.teamModel.findById({ _id: player.teamId });
      team.players = team.players.filter(
        playerId => playerId.toString() === player._id.toString(),
      );
      team.save();
      player.teamId = null;
      player.save();
    }

    return { success: false };
  }

  async attachPlayerToTeam(body: AttachPlayerToTeamDto): Promise<any> {
    const player = await this.playerModel.findById({ _id: body.playerId });
    const team = await this.teamModel.findById({ _id: body.teamId });

    if (player && team) {
      player.teamId = team._id;
      team.players.push(player._id);

      player.save();
      team.save();
    }

    // if (player.teamId) {
    //   const lastTeam = await this.teamModel.findById({ _id: player.teamId });
    //   lastTeam.players = team.players.filter(
    //     playerId => playerId.toString() === player._id.toString(),
    //   );
    //   lastTeam.save();
    //   player.teamId = team._id;
    //   player.save();
    // }

    // if (team.players.indexOf(player._id) === -1) {
    //   team.players.push(player);
    //   team.save();
    // }

    // this.teamModel.findOneAndUpdate(
    //   { _id: body.teamId },
    //   { $push: { players: player } },
    // );
    return { success: true };
  }

  async foo(body: any): Promise<any> {
    await this.unattachPlayerFromTeam(body.playerId);
    await this.attachPlayerToTeam(body);
    return { success: true };
  }

  async getAllTeams(): Promise<{ count: number; data: Team[] }> {
    // const data = await this.teamModel.find().exec();
    const data = await this.teamModel.aggregate([
      {
        $lookup: {
          from: 'players',
          localField: 'players',
          foreignField: '_id',
          as: 'players',
        },
      },
    ]);
    const count = await this.teamModel.countDocuments();
    return { data, count };
  }

  async getTeamById(id: string): Promise<OperationResponseDto<Team>> {
    return {
      success: true,
      response: await this.teamModel.findById({ _id: id }),
    };
  }
}
