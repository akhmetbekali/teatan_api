import { prop, Ref } from '@typegoose/typegoose';
import { Player } from '../players/player.model';

export class Team {
  @prop({ required: true })
  name: string;

  @prop()
  logoUrl: string;

  @prop()
  place: number;

  @prop()
  play: number;

  @prop()
  win: number;

  @prop()
  draw: number;

  @prop()
  lose: number;

  @prop()
  gf: number;

  @prop()
  ga: number;

  @prop()
  pts: number;

  @prop({ ref: () => [Player] })
  players: Ref<Player>[];
}
